const userModel = require("./models/userModel");
const userController = require("./controllers/user");

userModel
  .sync()
  .then(() => {
    console.log("Drop and re-sync db.");
  })
  .catch((error) => {
    console.log(error);
  });

// Add new user
const newUser = {
  name: "Shyam",
  email: "shyam@gmail.com",
  phone: "8272923244",
  password: "12345",
  active: 1,
  createdAt: "2021-01-08",
  updatedAt: "2021-01-08",
};

userController
  .createNewUser(newUser)
  .then((result) => console.log("User inserted successfully."))
  .catch((error) => console.log(error));

// Get user details
userController
  .findAllUsers()
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error.message);
  });

// update user
userController
  .updateUser(1, newUser)
  .then((result) => {
    console.log("result");
  })
  .catch((error) => {
    console.log(error.message);
  });

// delete user
userController
  .deleteUser(1)
  .then((result) => {
    console.log("result");
  })
  .catch((error) => {
    console.log(error.message);
  });
