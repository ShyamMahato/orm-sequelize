const Users = require("../models/userModel");

const createNewUser = (obj) => {
  return new Promise((resolve, reject) => {
    Users.create(obj)
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const findAllUsers = () => {
  return new Promise((resolve, reject) => {
    Users.findAll()
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const updateUser = (id, data) => {
  return new Promise((resolve, reject) => {
    Users.update(data, {
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          resolve({
            message: "Users was updated successfully.",
          });
        } else {
          reject({
            message: `Cannot update Users with id=${id}. Maybe Users was not found or req.body is empty!`,
          });
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const deleteUser = (id) => {
  return new Promise((resolve, reject) => {
    Users.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          resolve({
            message: "Users was deleted successfully!",
          });
        } else {
          reject({
            message: `Cannot delete Users with id=${id}. Maybe Users was not found!`,
          });
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

module.exports = {
  createNewUser,
  findAllUsers,
  updateUser,
  deleteUser,
};
